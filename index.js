// https://github.com/marpple/FxJS/blob/master/API.md
const {L, C} = window._;
const log = console.log;

// todo: 랜덤 숫자 기능 [v]
// todo: 랜덤한 정수 요소 3개를 갖는 배열 함수 [v]
// todo: 생성한 숫자와 사용자의 입력값 비교 함수 [v]
//  - 각 자리 별로 비교하고, 같은 자리에 같은 숫자가 몇개 있는지 판별합니다. (스트라이크 갯수) [v]
//  - 각 자리 별로 비교하고, 다른 자리에 같은 숫자가 몇개 있는지 판별합니다. (볼 갯수) [v]
// todo: 게임 시작 버튼 만들기 [v]
// todo: 게임 시작 버튼을 클릭 했을때, 랜덤한 세 자리 숫자 만들기 (사용자에게 보여지지 않는다) [v]
// todo: 숫자 입력칸 만들기 [v]
// todo: 사용자가 엔터키를 클릭 했을때, 입력값이 세자리 숫자가 아닌 경우 경고창 띄워주기 [v]
// todo: 화면에 스트라이크와 볼의 갯수를 표기합니다. [v]
// todo: 사용자가 10회까지 시도할 수 있도록 제한합니다. [v]
// todo: 게임 재시작 버튼을 만들고, 재시작 할 수 있도록 합니다. [v]

const $ = {};
$.qs = (str) => document.querySelector(str);
$.qsa = (str) => document.querySelectorAll(str);

const qsaMapFn = (str, f) => _.go($.qsa(str), _.map(f))

const getRandomIntInclusive = (min, max) => () => {
    const {ceil, floor, random} = Math;
    const MIN = ceil(min);
    const MAX = floor(max);
    return floor(random() * (MAX - MIN + 1)) + MIN;
};

const getRandomArray = (min, max) => (arrayLength = 1) => () => _.map(getRandomIntInclusive(min, max), _.range(arrayLength));

const accrue = (a, b, f) => _.pipe(_.map(f(a, b)), _.reduce(_.add));
const compare = (a, b, f) => _.go(_.range(a.length === b.length ? a.length : 0), accrue(a, b, f));

/**
 * compare([1, 2, 3], [4, 5, 6], (a, b) => i => a[i] + b[i]); // 출력값: 21
 * let k = accrue([1, 2, 3], [4, 5, 6], (a, b) => i => a[i] + b[i]);
 * k([0,1,2]) // 출력값: 21
 */

const predicate = f => (array1, array2) => index => f(array1, array2, index) ? 1 : 0;

const calcScore = compareFunc => (array1, array2) => compare(array1, array2, compareFunc);

const calcCountBy = f => calcScore(predicate(f));

const createRandomArray = getRandomArray(0, 9);
const threeRandomArray = createRandomArray(3);

const ball = (a, b, i) => _.includes(a[i], b);
const strike = (a, b, i) => a[i] === b[i];

const calcBallCount = calcCountBy(ball);
const calcStrikeCount = calcCountBy(strike);

const changeDisabled = (ele, boolean) => typeof ele === 'object' ? ele.disabled = boolean : $.qs(ele).disabled = boolean
const textContent = (target, text) => target.textContent = text;

let info = {
    userInput: [],
    computerInput: threeRandomArray(),
    ballCount: 0,
    strikeCount: 0,
    play: 0,
    isPlay: false,
    isFinish: false,
}


const INPUT_NUMBER = '#input-number label input'
const RESTART_BTN = '#game-restart-btn'
const START_BTN = '#game-start-btn'
const COUNT = '#count';


$.qs(RESTART_BTN).addEventListener('click', () => {
    info = {
        ...info,
        play: 0,
        isFinish: false
    }
    changeDisabled(RESTART_BTN, true)
    changeDisabled(START_BTN, false)
    log(info)
});

$.qs(START_BTN).addEventListener('click', (e) => {
    if (info.play === 2) {
        changeDisabled(RESTART_BTN, false)
        changeDisabled(START_BTN, true)
        info = {
            ...info,
            play: 0,
            isFinish: true
        }
    }
    info = {
        ...info,
        isPlay: false
    }
    if (info.isFinish) info = {
        ...info,
        computerInput: threeRandomArray()
    }
    textContent(e.target, `게임 계속`)
    log(`computerInput: [${info.computerInput}]`);
    qsaMapFn(INPUT_NUMBER, input => {
        changeDisabled(input, false);
        input.value = ''
    })
    log(info)
})

$.qs("*").addEventListener('keydown', (e) => {
    if (e.keyCode !== 13) return;
    if (info.isPlay) return;
    if (info.isFinish) return;
    const userInput = _.go(qsaMapFn(INPUT_NUMBER, input => input.value), _.reduce((a, b) => a + b), (str) => str.length < 3 ? (log('경고창 띄어주기'), [0, 0, 0]) : Array.from(str), _.map(Number.parseInt))

    log(userInput)
    info = {
        ...info,
        userInput,
        ballCount: info.ballCount + calcBallCount(userInput, info.computerInput),
        strikeCount: info.strikeCount + calcStrikeCount(userInput, info.computerInput),
        play: info.play + 1,
        isPlay: true,
    }

    $.qs(COUNT).innerHTML = `
        <h2>경기 횟수: ${info.play}</h2>
        <h2>스트라이크 갯수: ${info.strikeCount}</h2>
        <h2>볼 갯수: ${info.ballCount}</h2>
    `

    log(info)
})